import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: ['p { color: white; background-color: grey; padding: 15px; text-align: center; width: 100px}']
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
